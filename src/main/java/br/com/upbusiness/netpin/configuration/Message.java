package br.com.upbusiness.netpin.configuration;

import lombok.*;

@Data
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Message {

    private String message;
}
